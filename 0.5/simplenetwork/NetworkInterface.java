/*
 * Java Simple network library
 * Copyright (C) February 2018, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import simplenetwork.extendedStream.ExtendedDataInputStream;
import simplenetwork.extendedStream.ExtendedDataOutputStream;
import threadManagement.Killable;

/**
 * This is an object used to hold a connection,
 * it can either be created, and connected to a server,
 * or this object is created by SimpleNetworkServer when a client joins.
 * @author wilson
 */
public abstract class NetworkInterface implements Killable {
    private Socket socket; // The socket used for this classes communication
    final Object sync = new Object(); // The synchronization object used internally
    volatile boolean connected = false; // Whether this is connected to anything
    private IOException recentException; // The most recent exception that has been thrown
    ExtendedDataInputStream inputStream;
    ExtendedDataOutputStream outputStream;
    
    private SimpleNetworkServer networkServer;
    
    
    /**
     * These NetworkInterfaceChangeHandler instances are run when the network
     * interface connects to something,
     * 
     * NetworkInterfaceChangeHandler process methods MUST NOT TAKE A LONG TIME TO EXECUTE.
     * 
     * They are run:
     * - After the socket is connected to the server.
     * - After the onConnected() method is run.
     * - After the outputStreams are initiated, so messages can be sent from within the NetworkInterfaceChangeHandler.
     * - Before the read thread is started, so dataHandlers can be added (if it is a ControlledNetworkInterface).
     */
    private final LinkedList<NetworkInterfaceChangeHandler> connectHandlers = new LinkedList<NetworkInterfaceChangeHandler>();
    
    /**
     * These NetworkInterfaceChangeHandler instances are run after the connection is lost,
     * 
     * NetworkInterfaceChangeHandler process methods MUST NOT TAKE A LONG TIME TO EXECUTE.
     * 
     * This is run:
     * - After connected is set to false, and all resources are closed and nullified.
     * - After this is removed from the clients list of SimpleNetworkServer (if in server mode)
     * - After onDisconnected() is run.
     */
    private final LinkedList<NetworkInterfaceChangeHandler> leaveHandlers = new LinkedList<NetworkInterfaceChangeHandler>();
    
    
    
    /**
     * This is a unique ID assigned to each NetworkInterface.
     * This is only relevant for the server side of stuff. This ID is assigned by SimpleNetworkServer when
     * the connection is created, and is not used for the client side of stuff.
     */
    public final long id;
    
    
    /**
     * The ID can be optionally assigned, so if you don't want to assign it,
     * use this constructor and it will be set to -1, this should be done for client stuff.
     */
    public NetworkInterface() {
        id = -1l;
    }
    /**
     * The ID can be assigned for the server side of stuff, so there is an alternative
     * constructor to assign it.
     * @param id 
     */
    public NetworkInterface(long id) {
        this.id = id;
    }
    
    
    
    
    /**
     * This is run when a message is send to this, from the client/server it is connected to.
     * @param type The numeric message type.
     * @param data The data - a byte[], this can be null if an empty message was sent.
     */
    public abstract void recieve(int type, byte[] data);
    
    /**
     * This is run when this becomes connected to the supplied socket etc,
     * this can be used by implementing classes to set up threads that send stuff,
     * This is run before the read thread is started, so the network interface
     * cannot start receiving until this has finished executing,
     * THIS SHOULD NOT BE A BLOCKING CALL.
     */
    public abstract void onConnected();
    
    /**
     * This is run when connection is lost,
     * THIS SHOULD NOT BE A BLOCKING CALL.
     * 
     * This is run:
     * - After this is removed from the clients list of SimpleNetworkServer (if in server mode)
     */
    public abstract void onDisconnected();
    
    /**
     * Sends the defined message.
     * @param id The int type for the message - this will be received at the other end.
     * @param data The actual data - as a byte[].
     * @throws IOException Thows an IOException if anything goes wrong - it is up to the program to handle this.
     */
    public void send(int id, byte[] data) throws IOException {
        if (isConnected())
            synchronized(sync) {
                if (data == null) System.out.println("NULL DATA");
                if (outputStream == null) System.out.println("NULL STREAM");
                outputStream.writeInt(id);              // Send the supplied message TYPE
                outputStream.writeInt(data.length);     // Send the length of the message (in bytes)
                outputStream.write(data);               // Send the actual data
                outputStream.flush();                   // Flush it.
            }
    }
    
    /**
     * Sends an empty message - this can be used for requests etc.
     * @param id The int type for the message - this will be received at the other end.
     * @throws IOException Thows an IOException if anything goes wrong - it is up to the program to handle this.
     */
    public void send(int id) throws IOException {
        if (isConnected())
            synchronized(sync) {
                outputStream.writeInt(id);              // Send the supplied message TYPE
                outputStream.writeInt(0);    // Send the length of the message (in bytes)
                outputStream.flush();                   // Flush it.
            }
    }
    
    /**
     * Connects this NetworkInterface to the given address, returning whether successful,
     * if successful the read thread will be started,
     * if unsuccessful the read thread will not be started, false will be returned, and this method can be run again,
     * if already connected, this method will return false, and do nothing.
     * @param address The address to connect to.
     * @param port The host port to use.
     * @return Returns whether successful.
     */
    public boolean connectToServer(String address, int port) {
        synchronized(sync) {
            if (connected) return false;
            try {
                socket = new Socket(address,port);
                //inputStream = new ExtendedDataInputStream(socket.getInputStream());
                //outputStream = new ExtendedDataOutputStream(socket.getOutputStream());
                inputStream = new ExtendedDataInputStream(new BufferedInputStream(socket.getInputStream()));
                outputStream = new ExtendedDataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                connected = true;
                
                onConnected();
                for (NetworkInterfaceChangeHandler connectHandler : connectHandlers)
                    connectHandler.process(this);
                
                readThread.start();
                return true;
            } catch(IOException io) {
                recentException = io;
            }
        }
        kill(); return false;
    }
    
    /**
     * Connects this NetworkInterface to the given socket, returning whether successful,
     * this should be run by the SimpleNetworkServer, when a client connects,
     * this method is not public, so can ONLY be used internally,
     * if successful the read thread will be started,
     * if unsuccessful the read thread will not be started, false will be returned, and this method can be run again,
     * if already connected, this method will return false, and do nothing.
     * @param address The address to connect to.
     * @param port The host port to use.
     * @return Returns whether successful.
     */
    boolean connectToSocket(Socket socket, SimpleNetworkServer server) {
        synchronized(sync) {
            if (connected) return false;
            try {
                this.socket = socket;
                //inputStream = new ExtendedDataInputStream(socket.getInputStream());
                //outputStream = new ExtendedDataOutputStream(socket.getOutputStream());
                inputStream = new ExtendedDataInputStream(new BufferedInputStream(socket.getInputStream()));
                outputStream = new ExtendedDataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                connected = true;
                
                onConnected();
                for (NetworkInterfaceChangeHandler connectHandler : connectHandlers)
                    connectHandler.process(this);
                
                readThread.start();
                this.networkServer = server;
                return true;
            } catch(IOException io) {
                recentException = io;
            }
        }
        kill(); return false;
    }
    
    /**
     * Gets the most recent error.
     * @return The most recent IOException thrown and caught within this object's internal methods - e.g: connection error.
     */
    public IOException getRecentException() {
        synchronized(sync) {
            return recentException;
        }
    }
    
    /**
     * Kills the connection ready to re-connect later,
     * this can be run at any point, and will stop/interrupt the read thread.
     */
    @Override public void kill() {
        synchronized(sync) {
            connected = false;
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch(IOException io) {
                } finally {
                    inputStream = null;
                }
                
            if (outputStream != null)
                try {
                    outputStream.close();
                } catch(IOException io) {
                } finally {
                    outputStream = null;
                }
           
            if (socket != null)
                try {
                    socket.close();
                } catch(IOException io) {
                } finally {
                    socket = null;
                }
            readThread.interrupt();
            
            if (networkServer != null) networkServer.removeClientFromClientsList(this);
            
            onDisconnected();
            for (NetworkInterfaceChangeHandler leaveHandler : leaveHandlers)
                leaveHandler.process(this);
            
        }
        synchronized(killLock) { killLock.notify(); }
    }
    
    private final Object killLock = new Object();
    @Override public void waitForEnd() {
        while(connected)
            try { synchronized(killLock) { killLock.wait(); } } catch(InterruptedException ie) {}
    }
    
    @Override public boolean isRunning() { return connected; }
    
    public boolean isConnected() {
        synchronized(sync) {
            return connected;
        }
    }
    
    
    // ====================================== THE READ THREAD ========================================================
    
    
    
    private final ReadThread readThread = new ReadThread();
    
    private class ReadThread extends Thread {
        private ReadThread() {
            super("NetworkInterface read thread");
        }
        @Override public void run() {
            readMessages();
        }
    }
    
    /**
     * This is the content of the ReadThread - the code for reading data
     * from the input stream.
     * This is not private or final, because it must be re-implemented slightly
     * differently in ErrorDetectingNetworkInterface.
     */
    
    void readMessages() {
        try {
            while(connected) {
                try {
                    int msgtype = inputStream.readInt(),
                    msgLen = inputStream.readInt();
                    if (msgLen == 0) { recieve(msgtype,null); continue; }
                    if (msgLen < 0) System.err.println("MSGLEN: "+msgLen);
                    byte[] data = new byte[msgLen];
                    
                    //inputStream.read(data,0,msgLen); //24
                    
                    // NOTE: Read each byte individually, as Java's socket stream's
                    // read method that reads into a byte array mysteriously doesn't work
                    // sometimes. This way is more reliable.
                    int i = 0; while(i < msgLen) {
                        data[i] = inputStream.readByte();
                        i++;
                    }
                    recieve(msgtype,data);
                } catch(EOFException e) { // If socket closed, kill
                    if (connected) kill(); break;
                } catch(SocketException e) {
                    if (connected) kill(); break;
                }
            }
        } catch(IOException io) { io.printStackTrace(); }
    }
    
    
    
    
    // =========================== METHOD FOR ADDING AND REMOVING NetworkInterfaceChangeHandlers ===================
    
    public void addConnectHandler(NetworkInterfaceChangeHandler h) {
        synchronized(sync) {
            connectHandlers.add(h);
        }
    }
    public void addConnectHandlers(LinkedList<NetworkInterfaceChangeHandler> h) {
        synchronized(sync) {
            connectHandlers.addAll(h);
        }
    }
    public void addLeaveHandler(NetworkInterfaceChangeHandler h) {
        synchronized(sync) {
            leaveHandlers.add(h);
        }
    }
    public void addLeaveHandlers(LinkedList<NetworkInterfaceChangeHandler> h) {
        synchronized(sync) {
            leaveHandlers.addAll(h);
        }
    }
    
    
}
