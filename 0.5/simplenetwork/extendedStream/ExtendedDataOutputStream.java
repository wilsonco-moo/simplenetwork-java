/*
 * Java Simple network library
 * Copyright (C) February 2018, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork.extendedStream;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * The chunk data output stream is a simple extension to DataOutputStream.
 * 
 * It includes an extra method: writeDepthLong. This writes the long integer
 * num to the data stream, but cuts it to the length len (in bytes).
 * 
 * For example, writeDepthLong(64,3) would write the number 64 to the data output stream,
 * after shortening it to 3 bytes. This allows integers written to the DataOutputStream
 * to be specified to a precision from 1 to 8 bytes (inclusive).
 * 
 * NOTE: These methods *should* have no performance impact compared to the standard
 * readLong and writeLong methods, as they have been made as efficient as possible.
 * @author wilson
 */
public class ExtendedDataOutputStream extends DataOutputStream implements ExtendedDataOutput {
    public ExtendedDataOutputStream(OutputStream out) {
        super(out);
    }
    
    /**
     * This writes the specified long integer to the data output stream, cutting
     * it's size to the specified (len) number of bytes.
     * For example, writeDepthLong(12,3) would write the number 12 to the stream, but cutting it
     * to the size of 3 bytes.
     * @param num
     * @param len
     * @throws IOException 
     */
    @Override public void writeDepthLong(long num, int len) throws IOException {
        int i = 0, e = len*8;
        while(i < len) {
            writeByte((int)(num >> (e-=8) ));
            i++;
        }
    }
    
    /**
     * This writes the specified long integer to the data output stream, cutting it's size
     * to the specified number of bytes.
     * The number of bytes to use is derived from the length of the given byte array.
     * The byte array is used within this method as a temporary buffer. This allows the
     * method to execute less quickly as less conversion is required, and fewer method
     * calls are used.
     * NOTE: This is only quicker if you are running this a large number of times, as the
     * same temporary array can be used. Otherwise the time to allocate a new small array
     * would outweigh the speed advantages.
     * @param num
     * @param temp
     * @throws IOException 
     */
    @Override public void writeDepthLong(long num, byte[] temp) throws IOException {
        int len = temp.length, i = 0, e = len*8;
        while(i < len)
            temp[i++] = (byte)(num >> (e-=8) );
        write(temp);
    }
    
    /**
     * This method writes the given string, by converting it to utf8 bytes,
     * then using the write byte array method.
     * @param str
     * @throws IOException 
     */
    @Override public void writeLengthString(String str) throws IOException {
        writeLengthByteArray(str.getBytes("UTF8"));
    }
    
    /**
     * This method writes the given byte array, by:
     * 1: Writing an integer relating to the length of the byte array (in bytes)
     * 2: Writing the byte array's data.
     * This way a byte array of variable length can be written, and the reader
     * knows how long it is. This is useful for example if sending a byte
     * representation of a string.
     * @param bytes
     * @throws IOException 
     */
    @Override public void writeLengthByteArray(byte[] bytes) throws IOException {
        writeInt(bytes.length);
        write(bytes);
    }
    
    /**
     * This method writes the given byte array, by:
     * 1: Writing an integer relating to the length of the byte array (in bytes)
     * 2: Writing the byte array's data.
     * This way a byte array of variable length can be written, and the reader
     * knows how long it is. This is useful for example if sending a byte
     * representation of a string.
     * 
     * This method includes functionality to only send part of the byte array,
     * by specifying an offset and a length.
     * 
     * @param bytes
     * @param off
     * @param length
     * @throws IOException 
     */
    @Override public void writeLengthByteArray(byte[] bytes, int off, int length) throws IOException {
        writeInt(length);
        write(bytes,off,length);
    }
    
    
    
    
    
    
    
    /**
     * This method writes the given int array, by:
     * 1: Writing an integer relating to the length of the int array (in number of elements)
     * 2: Writing the int array's data.
     * This way an int array of variable length can be written, and the reader
     * knows how long it is. Then a copy of the array can be reconstructed
     * from the data.
     * @param ints The integer array to write.
     * @throws IOException 
     */
    @Override public void writeLengthIntArray(int[] ints) throws IOException {
        writeInt(ints.length);
        for (int i : ints) {
            writeInt(i);
        }
    }
    
    /**
     * This method writes the given int array, by:
     * 1: Writing an integer relating to the length of the int array (in number of elements)
     * 2: Writing the int array's data.
     * This way an int array of variable length can be written, and the reader
     * knows how long it is. Then a copy of the array can be reconstructed
     * from the data.
     * 
     * This method includes functionality to only send part of the byte array,
     * by specifying an offset and a length.
     * 
     * @param ints The integer array to write.
     * @param off
     * @param len
     * @throws IOException 
     */
    @Override public void writeLengthIntArray(int[] ints, int off, int len) throws IOException {
        writeInt(len);
        int i = off; len += off;
        while(i < len) {
            writeInt(ints[i]);
        }
    }
}
