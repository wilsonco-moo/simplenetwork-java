/*
 * Java Simple network library
 * Copyright (C) February 2018, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork.extendedStream;

import java.io.DataOutput;
import java.io.IOException;

/**
 * The extended data output is an extension to java's defualt DataOutput, where it includes
 * extra methods for writing long integers of defined length, Strings and int arrays,
 * 
 * NOTE: FOR DETAILED DOCUMENTATION OF AN IMPLEMENTATION OF THESE METHODS, SEE ExtendedDataOutputStream.
 * 
 * @author wilson
 */
public interface ExtendedDataOutput extends DataOutput {
    
    public void writeDepthLong(long num, int len) throws IOException;
    public void writeDepthLong(long num, byte[] temp) throws IOException;
    
    public void writeLengthString(String str) throws IOException;
    
    public void writeLengthByteArray(byte[] bytes) throws IOException;
    public void writeLengthByteArray(byte[] bytes, int off, int len) throws IOException;
    
    public void writeLengthIntArray(int[] ints) throws IOException;
    public void writeLengthIntArray(int[] ints, int off, int len) throws IOException;
}
