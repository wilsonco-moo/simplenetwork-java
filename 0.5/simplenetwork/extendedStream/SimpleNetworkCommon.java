/*
 * Java Simple network library
 * Copyright (C) February 2018, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork.extendedStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Provides a few convenient static methods for conversion from bytes to long,
 * also some command-line reading methods, and proper mod methods.
 * @author wilson
 */
public class SimpleNetworkCommon {
    
    
    /**
     * This reads a line of input from the user, returning an empty string if the user
     * enters nothing, but returning null if an error occurs.
     * This method now automatically puts in a newline after the prompt string, for the convenience
     * of IDEs. If you do not wish a newline to be automatically added, see readNewLine.
     * @param prompt The string to prompt the user with.
     * @return Returns the text the user inputs.
     */
    public static String read(String prompt) {
        System.out.println(prompt);
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try {
            return br.readLine();
        } catch (IOException io) {
            io.printStackTrace(); return null;
        }
    }
    
    /**
     * This reads a line of input from the user, but the default string is returned
     * if an error occurs, or the user enters nothing.
     * This method now automatically puts in a newline after the prompt string, for the convenience
     * of IDEs. If you do not wish a newline to be automatically added, see readNewLine.
     * @param prompt The prompt string to ask the user.
     * @param defaultString The default string to return if the user enters nothing, or an error occurs.
     * @return Returns the string the user enters, otherwise, if they do not enter a string, this returns the default string.
     */
    public static String read(String prompt, String defaultString) {
        System.out.println(prompt);
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try {
            String s = br.readLine();
            return s.isEmpty() ? defaultString : s;
        } catch (IOException io) {
            io.printStackTrace(); return defaultString;
        }
    }
    
    
    /**
     * This method acts exactly the same as the default read(prompt) method, with no default string, except for
     * the fact no newline is automatically added. See the documentation for the read(prompt) method.
     * @param prompt
     * @return 
     */
    public static String readNoNewLine(String prompt) {
        System.out.print(prompt);
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try {
            return br.readLine();
        } catch (IOException io) {
            io.printStackTrace(); return null;
        }
    }
    
    /**
     * This method acts exactly the same as the default read(prompt, defaultString) method, with no default string, except for
     * the fact no newline is automatically added. See the documentation for the read(prompt, defaultString) method.
     * @param prompt
     * @param defaultString
     * @return 
     */
    public static String readNoNewLine(String prompt, String defaultString) {
        System.out.print(prompt);
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try {
            String s = br.readLine();
            return s.isEmpty() ? defaultString : s;
        } catch (IOException io) {
            io.printStackTrace(); return defaultString;
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    public static int mod(int a, int b) {
        int val = a%b;
        return (val < 0) ? b + val : val;
    }
    public static float mod(float a, float b) {
        float val = a%b;
        return (val < 0) ? b + val : val;
    }
    public static double mod(double a, double b) {
        double val = a%b;
        return (val < 0) ? b + val : val;
    }
    public static long mod(long a, long b) {
        long val = a%b;
        return (val < 0) ? b + val : val;
    }
    public static short mod(short a, short b) {
        int val = a%b;
        return (short)((val < 0) ? b + val : val);
    }
    public static byte mod(byte a, byte b) {
        int val = a%b;
        return (byte)((val < 0) ? b + val : val);
    }
    
    
    
    
    
    
    
    
    public static void longToBytes(long num, byte[] bytes) {
        int len = bytes.length, i = 0, e = len*8;           // len is byte[] length, i byte[] index count, e is bit position within long.
        while(i < len)                                      // Loop through byte array
            bytes[i++] = (byte)(num >> (e-=8) );            // Bit shift input right by position, then convert to byte, taking that bit of the input. (Incrementing also)
    }
    
    public static long bytesToLong(byte[] bytes) {
        int len = bytes.length, e = len*8, i = 1;        // len it byte[] length, e is bit position within long, i is byte[] index count.
        long out = (long)bytes[0] << (e-=8);             // Shift and get first byte, without processing, to preserve leading two's compliment bits.
        while(i < len)                                   // Loop through byte array
            out |= ((long)bytes[i++] & 255l) << (e-=8);  // Bitwise-or upon the output:
        return out;                                      //  -> The current byte converted to a long, and'ed with 255 to remove leading two's compliment bits
    }
    
    public static void longToBytes(long num, byte[] bytes, int offset, int length) {
        int e = length*8; length += offset;                         // len is byte[] length, i byte[] index count, e is bit position within long.
        while(offset < length)                                      // Loop through byte array
            bytes[offset++] = (byte)(num >> (e-=8) );               // Bit shift input right by position, then convert to byte, taking that bit of the input. (Incrementing also)
    }
    
    public static long bytesToLong(byte[] bytes, int offset, int length) {
        int e = length*8; length += offset;                      // len it byte[] length, e is bit position within long, i is byte[] index count.
        long out = (long)bytes[offset++] << (e-=8);              // Shift and get first byte, without processing, to preserve leading two's compliment bits.
        while(offset < length)                                   // Loop through byte array
            out |= ((long)bytes[offset++] & 255l) << (e-=8);     // Bitwise-or upon the output:
        return out;                                              //  -> The current byte converted to a long, and'ed with 255 to remove leading two's compliment bits
    }
}
