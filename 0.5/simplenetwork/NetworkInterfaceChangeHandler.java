/*
 * Java Simple network library
 * Copyright (C) February 2018, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork;

/**
 * These can be assigned to SimpleNetworkServer.
 * The process method is run each time a new client joins, or a client leaves, depending
 * on what this is listening to.
 * @author wilson
 */
public interface NetworkInterfaceChangeHandler {
    public void process(NetworkInterface intf);
}
