
# Java Simple network library

## Overview

This is a Java networking library that I designed between July 2017 and February 2018. I have used this in several
game projects.

**NOTE: This library depends another of my java libraries: [threadManagement](https://gitlab.com/wilsonco-moo/threadManagement-java).**

The library provides systems for both the client and the server, which automate all the manual usage of sockets. This allows networked
application to be developed very quickly, by using the `NetworkInterface` class as an abstraction for a socket. 

The library allows a client-server system to be easily created, by assigning events to different
types of messages, being passed between the client and the server. The server uses a one thread per client system,
which allows arbitrarily many clients to be connected at the same time.

## Server-side description

To design the server-side, create a `SimpleNetworkServer` instance. `SimpleNetworkServer` contains a
thread that waits for the connections of new clients. When a new client connects,
`SimpleNetworkServer` will create a new instance of `NetworkInterface`, which handles the connection with that client.
Each `NetworkInterface` instance contains a thread that waits for incoming messages from that client. The `SimpleNetworkServer`
holds a list of these instances, and erases each of them when their connection closes.

## Client-side description

To connect to a server, simply create a `NetworkInterface` instance, and run it's `connectToServer()`
method. This then works in exactly the same way as the server-side `NetworkInterface` objects.

## More about `NetworkInterface`

In the library, `NetworkInterface` is an abstraction for a socket. Instances of this class are used for two things:
 * On the server-side, each `NetworkInterface` instance represents a connection to a single client.
 * On the client-side, a single `NetworkInterface` instance is used to represent a connection to the server.

To send messages using a `NetworkInterface`, the send method can be used. This can send either a byte array, or nothing
(an empty message). Each message requires a numeric id, to specify what type of message it is. The library takes care
of writing the message to the internal socket.

To receive a message, the abstract method `recieve(int type, byte[] data)` can be overridden. This is called each time we
receive a message, with the byte array received, and the message type. The library takes care of reading the message from
the internal socket.

## More advanced behaviour: `ControlledNetworkInterface`

The `ControlledNetworkInterface` class improves upon `NetworkInterface` by providing **much** more convenient methods
to send and receive data.

**To recieve:** Use the `addHandler(int type, DataHandler handler)` method within `ControlledNetworkInteface`, with a `DataHandler` instance.
This acts as an event, where abstract method within the `DataHandler` instance is run whenever a message is received. Depending
on the type of `DataHandler` used, (e.g: a `StringDataHandler`, or an `ExtendedDataStreamDataHandler`), the byte array
will be converted into a more convenient format for the program to read.

**To send:** `ControlledNetworkInterface` has another method to send messages, compared to `NetworkInterface`. That method
is `void send(int type, SendObject sendObj)`. This takes an instance of `SendObject`, of which there are (currently) two types.
The most useful of which is `ExtendedDataStreamSendObject`. This instance allows almost any type of data to be written conveniently.

# Example program

Here is a simple example program using the network library. The program does the following:
 * The client sends a string `"Initial hello from client"` to the server, along with an int value zero.
 * The server receives this message, adds one to the value, and sends the value back, along with a string `"Hello from client, value: " + value`.
 * The client receives this message, and sends it to the server again, along with a string `"Hello from client, value: " + value`.
 * This cycle repeats until the server receives a value which is 10 or greater, at which point the connection to the client is closed
   using the `kill()` method from `NetworkInterface`.

Notice that since both the client and the server use the `ControlledNetworkInteface` class, the library allows code to be written the same
way, regardless of whether the code is for the client or the server.

## Screenshots

Here is a screenshot of the server program running. Notice how the server program does not end, instead it waits for more clients to connect.

![Image](screenshots/demoServer.png)

Here is a screenshot of the client program running.

![Image](screenshots/demoClient.png)

## Server code

```java
package simplenetworkdemo;

import java.io.IOException;
import simplenetwork.ControlledNetworkInterface;
import simplenetwork.NetworkInterface;
import simplenetwork.SimpleNetworkServer;
import simplenetwork.datahandlertypes.generic.ExtendedDataStreamDataHandler;
import simplenetwork.extendedStream.ExtendedDataInputStream;
import simplenetwork.sendobjecttypes.ExtendedDataStreamSendObject;

public class DemoServer {
    public static void main(String[] args) {
        
        // Create a SimpleNetworkServer instance.
        SimpleNetworkServer server = new SimpleNetworkServer() {
            // This method is run each time a client connects, and should return
            // the custom NetworkInterface used for each client.
            @Override public NetworkInterface createInterface(long id) {
                // Create a custom ControlledNetworkInterface
                ControlledNetworkInterface netIntf = new ControlledNetworkInterface(id) {
                    // This abstract method is run after is becomes possible to send messages, but before the
                    // read thread is started.
                    @Override public void onConnected() {
                        System.out.println("A client has joined.");
                    }
                    // This is run when the client disconnects.
                    @Override public void onDisconnected() {
                        System.out.println("A client has left.");
                    }
                };
                
                // Add an ExtendedDataStreamDataHandler to message id 0, this will be run each time we receive
                // a message id 0.
                netIntf.addHandler(0, new ExtendedDataStreamDataHandler() {
                    @Override public void process(ExtendedDataInputStream data) {
                        try {
                            // Read a string and an int, print the string we have received.
                            System.out.println("Message from client: " + data.readLengthString());
                            int value = data.readInt() + 1;
                            if (value >= 10) {
                                // End the connection after the value reaches 10. The NetworkInterface's kill method
                                // should be used to close the connection cleanly.
                                netIntf.kill();
                            } else {
                                // Create a send object. This type of send object allows data to be written to it
                                // conveniently.
                                ExtendedDataStreamSendObject sendObj = new ExtendedDataStreamSendObject();
                                // Write a string, and the value.
                                sendObj.writeLengthString("Hello from server, value: " + value);
                                sendObj.writeInt(value);
                                // Send the send object, using the ControlledNetworkInterface send method.
                                netIntf.send(0, sendObj);
                            }
                        } catch (IOException ex) {
                            System.out.println("Error reading from message");
                        }
                    }
                });
                // Return our newly created custom NetworkInterface.
                return netIntf;
            }
        };
        
        // Listen to port 64000.
        if (server.listen(64000)) {
            // If listening to the port was successful, wait until the server ends, i.e: When the thread the
            // server uses to listen for incoming connection has ended. In this program that may never happen.
            server.waitForEnd();
            System.out.println("Server program has finished.");
        } else {
            // If listening to that port failed, another program may already be listening on that port for example:
            System.out.println("Listening to port 64000 failed.");
        }
    }
}
```

## Client code

```java
package simplenetworkdemo;

import java.io.IOException;
import simplenetwork.ControlledNetworkInterface;
import simplenetwork.datahandlertypes.generic.ExtendedDataStreamDataHandler;
import simplenetwork.extendedStream.ExtendedDataInputStream;
import simplenetwork.sendobjecttypes.ExtendedDataStreamSendObject;

public class DemoClient {
    public static void main(String[] args) {
        
        // Create our custom ControlledNetworkInterface, this represents the connection
        // to the server.
        ControlledNetworkInterface netIntf = new ControlledNetworkInterface() {
            // This abstract method is run after we after is becomes possible to send messages, (when we have
            // connected to the server), but before the read thread is started. We will use this method to
            // send the first message to the server, to start the back-and-fourth series of messages.
            @Override public void onConnected() {
                System.out.println("Connected to server.");
                // Create a send object, and write a message and an int value to it.
                ExtendedDataStreamSendObject sendObj = new ExtendedDataStreamSendObject();
                sendObj.writeLengthString("Initial hello from client");
                sendObj.writeInt(0);
                // Send this send object, using message id 0.
                try {
                    send(0, sendObj);
                } catch(IOException io) {
                    System.out.println("Error sending message.");
                }
            }
            // This abstract method is run when we lose the connection to the server.
            @Override public void onDisconnected() {
                System.out.println("Disconnected from server.");
            }
        };
        
        // Add an ExtendedDataStreamDataHandler to message id 0, this will be run each time we receive
        // a message id 0.
        netIntf.addHandler(0, new ExtendedDataStreamDataHandler() {
            @Override public void process(ExtendedDataInputStream data) {
                try {
                    // Read a string and an int, print the string we have received.
                    System.out.println("Message from server: " + data.readLengthString());
                    int value = data.readInt();
                    // Create a send object. This type of send object allows data to be written to it
                    // conveniently.
                    ExtendedDataStreamSendObject sendObj = new ExtendedDataStreamSendObject();
                    // Write a string, and the value.
                    sendObj.writeLengthString("Hello from client, value: " + value);
                    sendObj.writeInt(value);
                    // Send the send object, using the ControlledNetworkInterface send method.
                    netIntf.send(0, sendObj);
                } catch (IOException ex) {
                    System.out.println("Error reading from message");
                }
            }
        });
        
        // Connect the network interface to the server.
        if (netIntf.connectToServer("127.0.0.1", 64000)) {
            // If the connection was successful, wait until the connection has ended.
            netIntf.waitForEnd();
            System.out.println("Client program has finished.");
        } else {
            // If the connection fails.
            System.out.println("Connection failed.");
        }
    }
}
```
