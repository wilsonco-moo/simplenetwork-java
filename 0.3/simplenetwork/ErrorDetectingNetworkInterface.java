/*
 * Java Simple network library
 * Copyright (C) November 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork;


import java.io.EOFException;
import java.io.IOException;
import java.net.SocketException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Random;
import simplenetwork.extendedStream.ExtendedDataInput;
import simplenetwork.extendedStream.ExtendedDataOutput;

/**
 * This class is an extension to ControlledNetworkInterface. It adds an
 * acknowledgement system to ensure that required messages actually arrive at
 * to the receiver. If they don't, they are resent. Between being sent and
 * acknowledged, the messages are cached in a HashMap.
 * 
 * For this class, the user implementation MUST use the pinging system
 * for error detection and acknowledgement to work. See the documentation for ControlledNetworkInterface.
 * 
 * 
 * !!! WARNING - This class is spaghetti code, and how it works is extremely
 *               difficult to follow, and very confusingly complicated.
 * !!! TODO    - MAKE THIS CODE MORE READABLE.
 * 
 * @author wilson
 */
public abstract class ErrorDetectingNetworkInterface extends ControlledNetworkInterface {
    
    // =============================================================================================
    //        Stuff for managing caching of sent messages, and resending sent messages.
    
    /**
     * This stores outgoing acknowledged messages, until they are acknowledged by the other side.
     */
    private final HashMap<Long,OutgoingMessageCacheObject> outgoingCachedMessages = new HashMap<Long,OutgoingMessageCacheObject>();
    
    /**
     * This stores a list of the acknowledgement ids of missed incoming messages.
     */
    private final LinkedList<Long> incomingMissedMessageIds = new LinkedList<Long>();
    
    /**
     * This stores the acknowledgement id of the most recent incoming message.
     * This can be incremented at any time by the read thread. This is also read by
     * the write thread, at any time. Thus, this should be volatile but no synchronisation is
     * needed.
     */
    private volatile long incomingMostRecentId = 0;
    
    
    
    /**
     * This is the minimum amount of milliseconds before a message can be
     * resent, by finding it's id from the incoming list of missed id's.
     */
    public volatile long missedResendTime = 150;
    
    /**
     * This is the minimum amount of milliseconds before a message can be
     * resent, by comparing the outgoingAcknowledgementId with the most
     * recent outgoing acknowledgement id the other side has said they
     * have received.
     */
    public volatile long differenceResendTime = 1000;
    
    
    
    
    /**
     * This subclass stores an outgoing message, along with it's message type, and a timestamp.
     * This allows the outgoing message to be cached, so it can be stored later.
     * Note: The acknowledgement id does not need to be stored, as it stored in
     * the HashMap.
     */
    private static class OutgoingMessageCacheObject {
        private final int type;
        private final byte[] data;
        private volatile long lastSend;
        private OutgoingMessageCacheObject(int type, byte[] data, long time) {
            this.type = type; this.data = data; this.lastSend = time;
        }
        private OutgoingMessageCacheObject(int type, byte[] data) {
            this.type = type; this.data = data; lastSend = System.currentTimeMillis();
        }
    }
    
    
    /**
     * This writes the ACKNOWLEDGEMENT DATA for an outgoing message to the given data output. This data
     * is included in every message sent to the server, even unacknowledged messages.
     * It contains:
     * 1: The most recent acknowledgement id received. (long)
     * 2: The The number of missed messages. (int)
     * 3: A list of long integers representing the id's of the missed messages.
     * @param data 
     */
    private void outgoingWriteAcknowledgementData(ExtendedDataOutput data) throws IOException {
        data.writeLong(incomingMostRecentId); // Write the most recent message id.
        data.writeInt(incomingMissedMessageIds.size());      // Write the number of missed messages
        for (long l : incomingMissedMessageIds)            // Loop through all the missed messages
            data.writeLong(l);                 // Write the ID of each one.
    }
    
    
    
    private void outgoingManageMissedMessage(long ackId, long resendTime) throws IOException {
        if (outgoingCachedMessages.containsKey(ackId)) {
            OutgoingMessageCacheObject msg = outgoingCachedMessages.get(ackId);
            long time = System.currentTimeMillis();
            if (time - msg.lastSend > resendTime) {
                msg.lastSend = time;
                resendChecked(msg.type,ackId,msg.data);
            }
        }
    }
    
    
    
    // =============================================================================================
    //      Stuff for reading acknowledgement data
    
    
    // These fields control the confirmMessage() method.
    private long currentIncomingMessageConfirmationId = -1l;
    private boolean hasConfirmedThisIncomingMessage = false;
    private final Object incomingMessageConfirmationSync = new Object();
    
    /**
     * Within an error-checked data handler, another message cannot be sent
     * until this is run,
     * as otherwise acknowledgement for the current message will not be sent.
     * This method cannot be run at any other time, because otherwise an UnsupportedOperationException will be thrown.
     */
    protected void confirmMessage() {
        synchronized(incomingMessageConfirmationSync) {
            if (currentIncomingMessageConfirmationId == -1l)
                throw new UnsupportedOperationException("Cannot run the confirmMessage() method outside an error-checked data handler read method.");
            incomingConfirmAcknowledgement(currentIncomingMessageConfirmationId);
            hasConfirmedThisIncomingMessage = true;
        }
    }
    
    
    /**
     * The logic for the contents of the read-messages thread must also be
     * overridden, because the messages are now in a different format, as they
     * also store acknowledgement data,
     * 
     * NOTE: WITHIN THE MESSAGE HANDLER FOR A CONFIRMED MESSAGE, ANOTHER
     * MESSAGE CANNOT BE SENT, WITHOUT FIRST RUNNING confirmMessage(),
     * Because otherwise it will seem to the other side that you have
     * not actually received the message that the handler is associated with yet.
     * 
     */
    @Override void readMessages() {
        try {
            while(connected) {
                try {
                    try {
                        // Get the message type and length
                        int msgtype = inputStream.readInt();
                        
                        //System.out.println("Got message type: "+msgtype);//12
                        
                        int msgLen = inputStream.readInt();
                        
                        //System.out.println("Got message length: "+msgLen);//12
                        
                        
                        // Handle the acknowledgement data in the message
                        long incomingAckId = getAcknowledgementId(inputStream);
                        
                        
                        //System.out.println("Got incoming ackid: "+incomingAckId);//12
                        
                        
                        
                        // If should discard message, continue loop and wait for next message.
                        if (!shouldReadMessage(incomingAckId, inputStream, msgLen)) continue;
                        
                        
                        //System.out.println("Accepting message");//12
                        
                        
                        
                        // Take care of any missed messages.
                        incomingHandleMissedMessages(inputStream);
                        
                        
                        synchronized(incomingMessageConfirmationSync) {
                            currentIncomingMessageConfirmationId = incomingAckId;
                            
                            // Do the message receive stuff.
                            if (msgLen == 0) recieve(msgtype,null);
                            else {
                                byte[] data = new byte[msgLen];
                                //inputStream.read(data,0,msgLen); //24

                                // NOTE: Read each byte individually, as Java's socket stream's
                                // read method that reads into a byte array mysteriously doesn't work
                                // sometimes. This way is more reliable.
                                int i = 0; while(i < msgLen) {
                                    data[i] = inputStream.readByte();
                                    i++;
                                }
                                
                                recieve(msgtype,data);
                            }
                        
                        
                        
                            //System.out.println("Got data");//12

                            // Confirm the acknowledgement if no InvalidMessageExceptions have been thrown,
                            // and ackId isn't -1 (handleAcknowledgementData() returns -1 if the received
                            // message is not an acknowledged message.
                            if (incomingAckId != -1 && !hasConfirmedThisIncomingMessage) incomingConfirmAcknowledgement(incomingAckId);

                            hasConfirmedThisIncomingMessage = false;
                            currentIncomingMessageConfirmationId = -1l;
                        }
                        
                    } catch(InvalidMessageException ms) {
                        // Print an error message if the message has invalid content.
                        System.err.println("GENERAL NETWORK ERROR: Invalid message content: "+ms.getMessage());
                    }
                } catch(EOFException e) { // If socket closed, kill
                    if (connected) kill(); break;
                } catch(SocketException e) {
                    if (connected) kill(); break;
                }
            }
        } catch(IOException io) { io.printStackTrace(); }
    }
    
   
    /**
     * This method gets the acknowledgement id of the message.
     * @param data
     * @return Returns the acknowledgement id, or -1 if it is not an acknowledged message.
     * @throws IOException 
     */
    private long getAcknowledgementId(ExtendedDataInput data) throws IOException {
        long ackId = data.readByte();
        if (ackId >= 0) {
            ackId <<= 56;
            return ackId | data.readDepthLong(7);
        } else return -1l;
    }
    
    /**
     * Returns true if the rest of the message should be read,
     * otherwise, this method discards the rest of the message.
     * This method also removes the id from the incomingMissedMessageIds list
     * incomingAckId it is a member.
     * @param id
     * @return
     * @throws SocketException 
     */
    private boolean shouldReadMessage(long incomingAckId, ExtendedDataInput data, int messageLength) throws IOException {
        if (incomingAckId == -1 || incomingAckId > incomingMostRecentId) return true;
        if (incomingMissedMessageIds.contains(incomingAckId)) {
            incomingMissedMessageIds.remove(incomingAckId); // If it is a message that has been missed, remove it from the missed messages list.
            return true;
        }
        //System.out.println("Discarding message");//12
        // If shouldn't read message, discard it:
        data.readLong(); // Read the most recent outgoing message id recieved by other side.
        int missed = data.readInt();
        int i = 0; while(i < missed) { // Just read all the data, do nothing with it.
            data.readLong();
            i++;
        }
        i = 0; while(i < messageLength) {
            data.readByte();
            i++;
        }
        return false;
    }
    
    
    
    
    private final HashMap<Long,Boolean> tempMissedMap = new HashMap<Long,Boolean>();
    /**
     * This method does this:
     * 1: Reads the most recent ack id that the other side has received
     * 2: Reads the number messages the other side has missed
     * 3: Reads all the ids of the messages the other side has missed
     * 4: Looks through all cached messages: If any have an id older than
     *    the recent ack id that the other side has received, AND they are
     *    NOT included in the list of missed messages, remove them from the cached
     *    messages hashmap
     * 5: Run the outgoingManageMissedMessage method on all message ids
     *    the other side has missed.
     * @param data
     * @return 
     */
    private void incomingHandleMissedMessages(ExtendedDataInput data) throws IOException {
        // NOTE: Synchronize with sync here for the entire method,
        // as this method mostly involves modifying and looping through outgoingCachedMessages.
        // This is modified in the send methods (which are run in a different thread.
        synchronized(sync) {
        

            long mostRecentOutgoingReceived = data.readLong();

            //System.out.println("Got most recent outgoing other side has received: "+mostRecentOutgoingReceived);//12

            int missed = data.readInt();

            //System.out.println("Got number of missed messages: "+missed);//12

            int i = 0; while(i < missed) {
                tempMissedMap.put(data.readLong(), true);
                //System.out.println("Got missed: "+i+", of "+missed);//12
                i++;
            }
            LinkedList<Long> cachedMessagesToRemove = new LinkedList<Long>();
            for (Entry<Long,OutgoingMessageCacheObject> msg : outgoingCachedMessages.entrySet()) {
                long k = msg.getKey();
                if (k == mostRecentOutgoingReceived) {
                    cachedMessagesToRemove.add(k);
                } else if (k < mostRecentOutgoingReceived && !tempMissedMap.containsKey(k)) {
                    cachedMessagesToRemove.add(k);
                }
            }


            for (long k : tempMissedMap.keySet()) {
                outgoingManageMissedMessage(k,missedResendTime);
            }

            long id = mostRecentOutgoingReceived + 1;
            while(id <= outgoingAcknowledgementId) {
                outgoingManageMissedMessage(id,differenceResendTime);
                id++;
            }

            tempMissedMap.clear();



            if (cachedMessagesToRemove.isEmpty()) return; // Do nothing if none are to be removed.

            for (long k : cachedMessagesToRemove) {
                outgoingCachedMessages.remove(k);
            }

        }
    }
    
    /**
     * This confirms acknowledgement of an incoming message id.
     * This updates the incomingMostRecentId, and logs any messages that
     * have been missed.
     * @param id 
     */
    private void incomingConfirmAcknowledgement(long id) {
        synchronized(sync) {
            long i = incomingMostRecentId + 1;
            while(i < id) {
                //System.out.println("MISSED: "+i);//12
                incomingMissedMessageIds.add(i); // Declare all messages in the gap between incomingMostRecentId and id,
                i++;                             // as having been missed.
            }
            // Set incomingMostRecentId to this id, only if it IS
            // ACTUALLY more recent than has been previously.
            if (id > incomingMostRecentId) incomingMostRecentId = id;
        }
    }
    
    
    // =============================================================================================
    //                           Stuff for actually sending the data.
    
    
    /**
     * This stores the acknowledgement id of the next message that will be SENT by this object.
     */
    private long outgoingAcknowledgementId = 1;
    
    
    /**
     * This sends a checked (acknowledged) message, and is guaranteed to be received by
     * the other side.
     * This method sends a SendObject.
     * 
     * NOTE: If this is being run from within an error-checked data handler, the
     * confirmMessage() method must be run first, or the other side will not get acknowledgement
     * for the current message.
     * 
     * @param type
     * @param sendObj
     * @throws IOException 
     */
    public void sendChecked(int type, SendObject sendObj) throws IOException {
        sendChecked(type, sendObj.getData());
    }
    
    /**
     * This sends a checked (acknowledged) message, and is guaranteed to be received by
     * the other side.
     * This method sends byte array of data.
     * 
     * NOTE: If this is being run from within an error-checked data handler, the
     * confirmMessage() method must be run first, or the other side will not get acknowledgement
     * for the current message.
     * 
     * @param id
     * @param data
     * @throws IOException 
     */
    public void sendChecked(int id, byte[] data) throws IOException {
        if (isConnected())
            synchronized(sync) {
                //if (new Random().nextInt(3) == 0) {
                outputStream.writeInt(id);                         // Send the supplied message TYPE
                outputStream.writeInt(data.length);                // Send the length of the message (in bytes)
                outputStream.writeLong(outgoingAcknowledgementId); // Send the acknowlegement id for this message.
                outgoingWriteAcknowledgementData(outputStream);    // Write the acknowledgement data to the stream.
                outputStream.write(data);                          // Send the actual data
                outputStream.flush();                              // Flush it.
                //}
                // Add the message to the cached messages.
                outgoingCachedMessages.put(outgoingAcknowledgementId++, new OutgoingMessageCacheObject(id,data));
            }
    }
    
    /**
     * This method is used internally to resend a message.
     * 
     * NOTE: If this is being run from within an error-checked data handler, the
     * confirmMessage() method must be run first, or the other side will not get acknowledgement
     * for the current message.
     * 
     * @param id
     * @param ackId
     * @param data
     * @throws IOException 
     */
    private void resendChecked(int id, long ackId, byte[] data) throws IOException {
        if (isConnected())
            synchronized(sync) {
                outputStream.writeInt(id);                      // Send the supplied message TYPE
                outputStream.writeInt(data.length);             // Send the length of the message (in bytes)
                outputStream.writeLong(ackId);                  // Send the acknowlegement id for this message.
                outgoingWriteAcknowledgementData(outputStream); // Write the acknowledgement data to the stream.
                outputStream.write(data);                       // Send the actual data
                outputStream.flush();                           // Flush it.
            }
    }
    
    /**
     * This sends a checked (acknowledged) message, and is guaranteed to be received by
     * the other side.
     * This method sends an empty message of the specified message type.
     * 
     * NOTE: If this is being run from within an error-checked data handler, the
     * confirmMessage() method must be run first, or the other side will not get acknowledgement
     * for the current message.
     * 
     * @param id
     * @throws IOException 
     */
    public void sendChecked(int id) throws IOException {
        if (isConnected())
            synchronized(sync) {
                outputStream.writeInt(id);                         // Send the supplied message TYPE
                outputStream.writeInt(0);                          // Send the length of the message (in bytes)
                outputStream.writeLong(outgoingAcknowledgementId); // Send the acknowlegement id for this message.
                outgoingWriteAcknowledgementData(outputStream);    // Write the acknowledgement data to the stream.
                outputStream.flush();                              // Flush it.
                // Add the message to the cached messages.
                outgoingCachedMessages.put(outgoingAcknowledgementId++, new OutgoingMessageCacheObject(id,new byte[0]));
            }
    }
    
    /**
     * Sends the defined message.
     * 
     * NOTE: If this is being run from within an error-checked data handler, the
     * confirmMessage() method must be run first, or the other side will not get acknowledgement
     * for the current message.
     * @param data
     * @throws IOException 
     */
    public void debug_sendRaw(byte[] data) throws IOException {
        if (isConnected())
            synchronized(sync) {
                outputStream.write(data);
                outputStream.flush();
            }
        
    }
    
    /**
     * Sends the defined message.
     * 
     * NOTE: If this is being run from within an error-checked data handler, the
     * confirmMessage() method must be run first, or the other side will not get acknowledgement
     * for the current message.
     * @param id
     * @param data
     * @throws IOException 
     */
    @Override public void send(int id, byte[] data) throws IOException {
        if (isConnected())
            synchronized(sync) {
                outputStream.writeInt(id);                      // Send the supplied message TYPE
                outputStream.writeInt(data.length);             // Send the length of the message (in bytes)
                outputStream.writeByte(-1);                     // Write a negative byte - it is not an acknowledged message.
                outgoingWriteAcknowledgementData(outputStream); // Write the acknowledgement data to the stream.
                outputStream.write(data);                       // Send the actual data
                outputStream.flush();                           // Flush it.
            }
    }
    
    /**
     * Sends an empty message - this can be used for requests etc.
     * 
     * NOTE: If this is being run from within an error-checked data handler, the
     * confirmMessage() method must be run first, or the other side will not get acknowledgement
     * for the current message.
     * 
     * @param id The int type for the message - this will be received at the other end.
     * @throws IOException Thows an IOException if anything goes wrong - it is up to the program to handle this.
     */
    @Override public void send(int id) throws IOException {
        if (isConnected())
            synchronized(sync) {
                outputStream.writeInt(id);                      // Send the supplied message TYPE
                outputStream.writeInt(0);                       // Send the length of the message (in bytes)
                outputStream.writeByte(-1);                     // Write a negative byte - it is not an acknowledged message.
                outgoingWriteAcknowledgementData(outputStream); // Write the acknowledgement data to the stream.
                outputStream.flush();                           // Flush it
            }
    }
}
