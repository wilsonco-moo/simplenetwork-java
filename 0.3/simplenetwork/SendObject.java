/*
 * Java Simple network library
 * Copyright (C) November 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork;

/**
 * This is the basis for data handlers - this should not be extended directly, as it
 * adds no additional functionality.
 * @author wilson
 */
public abstract class SendObject {
    protected volatile boolean sent = false;
    byte[] getData() {
        byte[] b = data();
        sent = true;
        return b;
    }
    protected abstract byte[] data();
}
