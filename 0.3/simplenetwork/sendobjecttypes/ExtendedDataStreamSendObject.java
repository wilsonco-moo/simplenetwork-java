/*
 * Java Simple network library
 * Copyright (C) November 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork.sendobjecttypes;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import simplenetwork.SendObject;
import simplenetwork.extendedStream.ExtendedDataOutput;
import simplenetwork.extendedStream.ExtendedDataOutputStream;

/**
 * This is a data send object that implements DataOutput.
 * This allows any data to be written to this send object. The data is stored
 * temporarily, then send when the send method is called.
 * 
 * It allows any primitive type to be written.
 * It also includes some methods to write:
 *  > Strings using writeLengthString
 *  > int arrays using writeLengthIntArray
 *  > byte arrays using writeLengthByteArray
 * @author wilson
 */
public class ExtendedDataStreamSendObject extends SendObject implements ExtendedDataOutput {
    
    private final ByteArrayOutputStream bytes;
    private final ExtendedDataOutputStream data;
    
    public ExtendedDataStreamSendObject() {
        bytes = new ByteArrayOutputStream();
        data = new ExtendedDataOutputStream(bytes);
    }
    
    public ExtendedDataStreamSendObject(int initialSize) {
        bytes = new ByteArrayOutputStream(initialSize);
        data = new ExtendedDataOutputStream(bytes);
    }
    
    
    @Override protected byte[] data() {
        if (sent) return null;
        byte[] b = bytes.toByteArray();
        try {
            data.close();
        } catch(IOException io) { io.printStackTrace(); }
        return b;
    }
    
    
    // ============================================ METHODS FROM DATA OUTPUT =============================
    
    
    @Override public void write(int i) {
        try {
            if (!sent) data.write(i);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void write(byte[] bytes) {
        try {
            if (!sent) data.write(bytes);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void write(byte[] bytes, int i, int i1) {
        try {
            if (!sent) data.write(bytes,i,i1);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void writeBoolean(boolean bln) {
        try {
            if (!sent) data.writeBoolean(bln);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void writeByte(int i) {
        try {
            if (!sent) data.writeByte(i);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void writeShort(int i) {
        try {
            if (!sent) data.writeShort(i);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void writeChar(int i) {
        try {
            if (!sent) data.writeChar(i);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void writeInt(int i) {
        try {
            if (!sent) data.writeInt(i);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void writeLong(long l) {
        try {
            if (!sent) data.writeLong(l);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void writeFloat(float f) {
        try {
            if (!sent) data.writeFloat(f);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void writeDouble(double d) {
        try {
            if (!sent) data.writeDouble(d);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void writeBytes(String string) {
        try {
            if (!sent) data.writeBytes(string);
        } catch(IOException io) { io.printStackTrace(); }
    }
    @Override public void writeChars(String string) {
        try {
            if (!sent) data.writeChars(string);
        } catch(IOException io) { io.printStackTrace(); }
    }

    @Override public void writeUTF(String string) {
        try {
            if (!sent) data.writeUTF(string);
        } catch(IOException io) { io.printStackTrace(); }
    }

    
    
    // =============================================== METHODS FROM EXTENDED DATA OUTPUT ======================
    
    
    
    
    @Override public void writeDepthLong(long num, int len) throws IOException {
        try {
            data.writeDepthLong(num, len);
        } catch(IOException io) { io.printStackTrace(); }
    }

    @Override public void writeDepthLong(long num, byte[] temp) throws IOException {
        try {
            data.writeDepthLong(num, temp);
        } catch(IOException io) { io.printStackTrace(); }
    }

    @Override public void writeLengthString(String str) throws IOException {
        try {
            data.writeLengthString(str);
        } catch(IOException io) { io.printStackTrace(); }
    }

    @Override public void writeLengthByteArray(byte[] bytes) throws IOException {
        try {
            data.writeLengthByteArray(bytes);
        } catch(IOException io) { io.printStackTrace(); }
    }

    @Override public void writeLengthByteArray(byte[] bytes, int off, int len) throws IOException {
        try {
            data.writeLengthByteArray(bytes,off,len);
        } catch(IOException io) { io.printStackTrace(); }
    }

    @Override public void writeLengthIntArray(int[] ints) throws IOException {
        try {
            data.writeLengthIntArray(ints);
        } catch(IOException io) { io.printStackTrace(); }
    }

    @Override public void writeLengthIntArray(int[] ints, int off, int len) throws IOException {
        try {
            data.writeLengthIntArray(ints,off,len);
        } catch(IOException io) { io.printStackTrace(); }
    }
    
}
