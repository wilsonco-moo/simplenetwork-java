/*
 * Java Simple network library
 * Copyright (C) November 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork.extendedStream;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * The chunk data input stream is a simple extension to DataInputStream.
 * 
 * It includes the method readDepthLong: this reads a long integer from the
 * DataInputStream, of the specified length in bytes.
 * 
 * For example, readDepthLong(3) would read the next 3 bytes of the dataInputStream,
 * and convert them into a Long integer. This allows integers within the DataInputStream
 * to be specified to a precision in bytes, anywhere from 1 to 8.
 * 
 * NOTE: These methods *should* have no performance impact compared to the standard
 * readLong and writeLong methods, as they have been made as efficient as possible.
 * 
 * 
 * The ChunkDataInputStream also includes the length String and array operations
 * previously included only with DataStreamDataHandlers and send objects,
 * for convenience.
 * @author wilson
 */
public class ExtendedDataInputStream  extends DataInputStream implements ExtendedDataInput {
    public ExtendedDataInputStream(InputStream in) {
        super(in);
    }
    
    /**
     * This method reads a long integer, of length len from the stream.
     * @param len
     * @return
     * @throws IOException 
     */
    @Override public long readDepthLong(int len) throws IOException {
        int e = len*8, i = 1;
        long out = (long)readByte() << (e-=8);
        while(i < len) {
            out |= ((long)readByte() & 255l) << (e-=8);
            i++;
        }
        return out;
    }
    
    /**
     * Allows a temporary byte array to be specified:
     * ??? This might make it slightly faster? if the array is reused a lot of times.
     * @param temp
     * @return
     * @throws IOException 
     */
    @Override public long readDepthLong(byte[] temp) throws IOException {
        int len = temp.length, e = len*8, i = 1;
        
        //read(temp, 0, len); //24 // Read all at once into the temporary array, then only one read method is called on the stream.
        int ii = 0; while(ii < len) {
            temp[ii] = readByte();
            ii++;
        }
        
        long out = (long)temp[0] << (e-=8);
        while(i < len) {
            out |= ((long)temp[i] & 255l) << (e-=8);
            i++;
        }
        return out;
    }
    
    
    
    
    
    /**
     * This method reads a String encoded as an int length, then a set of UTF8 bytes, stored
     * within the data input stream, such as by DataStreamSendObject.writeLengthString()
     * this only does something when run from within the abstract process method.
     * @return 
     * @throws java.io.IOException 
     */
    @Override public String readLengthString() throws IOException {
        return new String(readLengthByteArray(),"UTF8");
    }
    
    
    
    
    /**
     * This method reads a byte array encoded as an int length, then a series of bytes, stored
     * within the data input stream, such as by (Deprecated) DataStreamSendObject.writeLengthBytes()
     * this only does something when run from within the abstract process method.
     * @return Will return a new byte array.
     * @throws java.io.IOException 
     */
    @Override public byte[] readLengthByteArray() throws IOException {
        int len = readInt();
        byte[] bytes = new byte[len];
        
        //read(bytes,0,len); //24
        int i = 0; while(i < len) {
            bytes[i] = readByte();
            i++;
        }
        
        
        return bytes;
    }
    
    /**
     * This method reads a byte array encoded as an int length, then a series of bytes, stored
     * within the data input stream, such as by (Deprecated) DataStreamSendObject.writeLengthBytes()
     * this only does something when run from within the abstract process method.
     * 
     * NOTE: WILL THROW AN ARRAY INDEX OUT OF BOUNDS EXCEPTION IF THE ARRAY IS NOT BIG ENOUGH.
     * @param bytes The byte array to populate.
     * @throws java.io.IOException
     */
    @Override public void readLengthByteArray(byte[] bytes) throws IOException {
        int len = readInt();
        
        //read(bytes,0,len); // 24
        int i = 0; while(i < len) {
            bytes[i] = readByte();
            i++;
        }
        
    }
    
    /**
     * This method reads a byte array encoded as an int length, then a series of bytes, stored
     * within the data input stream, such as by (Deprecated) DataStreamSendObject.writeLengthBytes()
     * this only does something when run from within the abstract process method.
     * 
     * NOTE: WILL THROW AN ARRAY INDEX OUT OF BOUNDS EXCEPTION IF THE ARRAY IS NOT BIG ENOUGH.
     * @param bytes The byte array to populate.
     * @param offset The offset to write to
     * @throws java.io.IOException
     */
    @Override public void readLengthByteArray(byte[] bytes, int offset) throws IOException {
        int len = readInt();
        //read(bytes,offset,len); //24
        int i = offset; while(i < len) {
            bytes[i] = readByte();
            i++;
        }
    }
    
    
    
    
    /**
     * This method reads a int array encoded as an int length, then a series of integers, stored
     * within the data input stream, such as by DataStreamSendObject.writeLengthIntArray()
     * this only does something when run from within the abstract process method.
     * @return 
     * @throws java.io.IOException 
     */
    @Override public int[] readLengthIntArray() throws IOException {
        int len = readInt();
        int[] arr = new int[len];
        int i = 0; while(i < len) {
            arr[i] = readInt(); i++;
        }
        return arr;
    }
    
    /**
     * This method reads a int array encoded as an int length, then a series of integers, stored
     * within the data input stream, such as by DataStreamSendObject.writeLengthIntArray()
     * this only does something when run from within the abstract process method.
     * NOTE: WILL THROW AN ARRAY INDEX OUT OF BOUNDS EXCEPTION IF THE ARRAY IS NOT BIG ENOUGH.
     * @param arr The int array to populate.
     * @throws java.io.IOException 
     */
    @Override public void readLengthIntArray(int[] arr) throws IOException {
        int len = readInt();
        int i = 0; while(i < len) {
            arr[i] = readInt(); i++;
        }
    }
    
    /**
     * This method reads a int array encoded as an int length, then a series of integers, stored
     * within the data input stream, such as by DataStreamSendObject.writeLengthIntArray()
     * this only does something when run from within the abstract process method.
     * NOTE: WILL THROW AN ARRAY INDEX OUT OF BOUNDS EXCEPTION IF THE ARRAY IS NOT BIG ENOUGH.
     * @param arr The int array to populate.
     * @param offset The offset to use when populating the int array.
     * @throws java.io.IOException 
     */
    @Override public void readLengthIntArray(int[] arr, int offset) throws IOException {
        int len = readInt() + offset;
        int i = offset; while(i < len) {
            arr[i] = readInt(); i++;
        }
    }
}
