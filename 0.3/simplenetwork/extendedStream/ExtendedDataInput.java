/*
 * Java Simple network library
 * Copyright (C) November 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork.extendedStream;

import java.io.DataInput;
import java.io.IOException;

/**
 * This is an extension to Java's standard DataInput interface,
 * 
 * NOTE: FOR DETAILED DOCUMENTATION OF AN IMPLEMENTATION OF THESE METHODS, SEE ExtendedDataInputStream.
 * 
 * It includes extra methods for reading long integers of a specific number
 * of bytes, and methods to read Strings, and entire byte and int arrays.
 * @author wilson
 */
public interface ExtendedDataInput extends DataInput {
    
    public long   readDepthLong(int len) throws IOException;
    public long   readDepthLong(byte[] temp) throws IOException;
    
    public String readLengthString() throws IOException;
    
    public byte[] readLengthByteArray() throws IOException;
    public void   readLengthByteArray(byte[] bytes) throws IOException;
    public void   readLengthByteArray(byte[] bytes, int offset) throws IOException;
    
    public int[]  readLengthIntArray() throws IOException;
    public void   readLengthIntArray(int[] ints) throws IOException;
    public void   readLengthIntArray(int[] ints, int offset) throws IOException;
}
