/*
 * Java Simple network library
 * Copyright (C) December 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork;

import java.io.IOException;
import java.util.HashMap;
import simplenetwork.NetworkInterface;
import simplenetwork.datahandlertypes.generic.ExtendedDataStreamDataHandler;
import simplenetwork.datahandlertypes.generic.EmptyDataHandler;
import simplenetwork.extendedStream.ExtendedDataInputStream;
import simplenetwork.sendobjecttypes.ExtendedDataStreamSendObject;

/**
 * The ControlledNetworkInterface adds extra functionality to NetworkInterface.
 * 
 * Sending/receiving
 * -----------------
 * 
 * Instead of the send and receive systems handling rew byte[] data, (requiring user implementation
 * to manage), the ControlledNetworkInterface does this for you. To send data, create a SendObject,
 * write data to it, then use the ControlledNetworkInterface send(SendObject) method to send it.
 * 
 * To receive data, assign a DataHandler to a particular message Id. The data handler's
 * abstract process method will be automatically called each time a message is sent on that ID.
 * The DataHandler will also do conversion from byte[] to a more useful format.
 * 
 * 
 * Pinging
 * -------
 * 
 * NOTE: Message IDs 2147483647 and 2147483646, (Integer.MAX_VALUE(-1)) are RESERVED for pinging.
 * 
 * The ControlledNetworkInterface class contains two methods that can be used,
 * ping(), and checkForPing().
 * 
 * The pinging system sends a periodic message from server to client and back, around
 * each pingInterval, but less than each maxPingInterval.
 * 
 * If a message is not received in at most maxPingInterval, the connection is automatically
 * terminated and the kill method is run.
 * 
 * If this system is used, one side must periodically run the ping() method, and the other
 * must run the checkForPing() method. This can for example be done with a ThreadSchedule.
 * 
 * The method getPingValue() can be used, this returns the approximate round trip value
 * for connections. This will be zero if either pinging is not being run, or in the first
 * ~10 seconds where no ping messages have been sent yet.
 * 
 * NOTE: The pingInterval and maxPingInterval can be changed, as long as it is consistent
 * across the client and the server, and the methods are still using these values as the
 * periodic waiting time between running ping() and checkForPing().
 * 
 * 
 * Reliability
 * ^^^^^^^^^^^
    * 
    * Original design philosophy
    * --------------------------
    * 
    * A system could be implemented here to resend all messages that do not arrive properly.
    * However, Java uses TCP, which is supposed to ensure that all messages sent to the Java
    * Sockets arrive properly.* As such, a full error detection/resending system is not necessary
    * as TCP already provides one.**
    * 
    *     * In practice TCP does not fully ensure sent messages are actually received - only that they are correct.
    *    ** See the "After implementing the chunk system, and testing over unreliable connections" section.
    * 
    * 
    * However, given an unreliable connection, messages might not arrive properly. This (even with
    * TCP) can give very slow connections. This will probably be picked up by the ping system,
    * and automatically disconnect the client.
    * 
    * A note regarding the Chunk system that uses this networking library:
    * 
    *   As such, if the program is expected to run over an unreliable connection, and the ability
    *   to detect disconnects is not as important, DO NOT USE THE PING SYSTEM, or at least 
    *   increase the pingInterval and maxPingInterval.
    * 
    *   Also, a partial re-sending system is provided by the chunk system, where a re-request is
    *   sent if a chunk is not sent back within a given time. However, this produces errors in the
    *   console, as it can only happen if there is a connection error, or if the chunks are taking
    *   an abnormally long time to generate.
    * 
    *   This resending system is not strictly necessary, and can be disabled by setting the WorldRenderer's
    *   maximumReloads to zero.
    * 
    * 
    * After implementing the chunk system, and testing over unreliable connections
    * ----------------------------------------------------------------------------
    * 
    * (reference https://stackoverflow.com/a/6940010 )
    * 
    * When using this network library over an unreliable network connection, is has become clear that
    * TCP's error detection/correction is not enough. Whereas TCP ensures the messages are correct,
    * it does not include any form of acknowledgement that entire messages arrive. As such, in the 
    * chunk system, lots of chunks don't arrive, and are not resent until the default error
    * detection kicks in. This causes only some of the chunks to load, and the rest are only
    * received when re-requested.
    * 
    * This is not optimal, as there must be a method to ensure a message is received, and re-sent
    * if it isn't.
    * 
    * As such,
    * "A system could be implemented here to resend all messages that do not arrive properly."
    * IS NECESSARY. BUT, the user of the network library must be able to define which messages
    * require acknowledgement. Hence, the class ErrorDetectingNetworkInterface is required.
 * 
 * 
 * 
 * 
 * 
 * 
 * @author wilson
 */
public abstract class ControlledNetworkInterface extends NetworkInterface {
    private final HashMap<Integer,DataHandler> dataHandlers = new HashMap<Integer,DataHandler>();
    private final Object hashSync = new Object(); // The sync object for the dataHandlers hashmap
    
    @Override public void recieve(int type, byte[] data) {
        DataHandler handler;
        synchronized(hashSync) {
            handler = dataHandlers.get(type);
        }
        if (handler == null) {
            if (type == send || type == response) { // If it is a ping message
                initiatePinging(); // Automatically add the ping stuffs
                recieve(type,data); // Re-run the method, hopefully this shouldn't run into a StackOverflowException.
            } else //                                     """""""""
                new Exception("Invalid message ID: "+type).printStackTrace();
        } else {
            handler.run(data);
        }
    }
    
    // ==================================== DATA HANDLER MANAGEMENT ================================
    
    /**
     * This adds a DataHandler object, by making an entry in the dataHandlers hashmap.
     * @param type The integer message type for the handler.
     * @param handler The DataHandler object.
     */
    public void addHandler(int type, DataHandler handler) {
        synchronized(hashSync) {
            handler.type = type;
            dataHandlers.put(type, handler);
        }
    }
    
    /**
     * This removes a data handler object, by removing it from dataHandlers.
     * @param type The int message type object to remove.
     */
    public void removeHandler(int type) {
        synchronized(hashSync) {
            dataHandlers.get(type).type = -1;
            dataHandlers.remove(type);
        }
    }
    
    /**
     * Sends a SendObject.
     * @param type
     * @param sendObj
     * @throws IOException 
     */
    public void send(int type, SendObject sendObj) throws IOException {
        send(type, sendObj.getData());
    }
    
    
    // ====================================================== PINGING ==============================================================================
    
    public int pingInterval = 5000;
    public int maxPingInterval = 10000;
    
    private final Object pingSync = new Object();
    
    private final static int send = Integer.MAX_VALUE;
    private final static int response = Integer.MAX_VALUE-1;

    
    private volatile int mostRecentPingTime = 0; // The most recent ping round trip time (milliseconds).
    private volatile long mostRecentPingMessage = -1; // The most recent time at which a ping message was recieved (milliseconds).
    private volatile long mostRecentPingSend = -1; // The most recent time at which a ping message was sent (nanoseconds).
    
    /**
     * This should be run by one side each pingInterval.
     */
    public void ping() {
        long time = System.currentTimeMillis(); // Get the time
        if (mostRecentPingMessage == -1) mostRecentPingMessage = time; // If most recent ping message has not been activated yet, set to current time.
        if (time - mostRecentPingMessage > maxPingInterval) { // If not had anything for over maxPingInterval
            kill(); // Kill the connection
            return;
        }
        
        ExtendedDataStreamSendObject data = new ExtendedDataStreamSendObject();
        data.writeInt(mostRecentPingTime); // Assemble a send object, and send a most recent ping time
        mostRecentPingSend = System.nanoTime();
        try {
            send(send,data);
        } catch(IOException io) { System.out.println("Cannot send ping message"); }
    }
    /**
     * This should be run by the other side each pingInterval.
     */
    public void checkForPing() {
        long time = System.currentTimeMillis(); // Get the time
        if (mostRecentPingMessage == -1) mostRecentPingMessage = time; // If most recent ping message has not been activated yet, set to current time.
        if (time - mostRecentPingMessage > maxPingInterval) { // If not had anything for over maxPingInterval
            kill(); // Kill the connection
            return;
        }
    }
    
    
    private void initiatePinging() {
        synchronized(pingSync) {
            // Response to ping send
            addHandler(send,new ExtendedDataStreamDataHandler() {
                @Override public void process(ExtendedDataInputStream data) {
                    try {
                        send(response);
                    } catch(IOException io) { io.printStackTrace(); }
                    mostRecentPingMessage = System.currentTimeMillis();
                    try {
                        mostRecentPingTime = data.readInt();
                    } catch(IOException io) { io.printStackTrace(); }
                    //System.out.println("PING!! "+mostRecentPingTime+"ms");
                }
            });
            
            // Response to ping response
            addHandler(response,new EmptyDataHandler() {
                @Override public void process() {
                    long time = System.nanoTime();
                    mostRecentPingTime = (int)((time - mostRecentPingSend)/1000000);
                    mostRecentPingMessage = System.currentTimeMillis();
                    //System.out.println("PING!! "+mostRecentPingTime+"ms");
                }
            });
        }
    }
    
    public int getPingValue() {
        return mostRecentPingTime;
    }
}
