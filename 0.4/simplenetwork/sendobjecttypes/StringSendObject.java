/*
 * Java Simple network library
 * Copyright (C) December 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork.sendobjecttypes;

import java.io.UnsupportedEncodingException;
import simplenetwork.SendObject;

/**
 * A send object that sends a single string, for reading by the StringDataHandler.
 * @author wilson
 */
public class StringSendObject extends SendObject  {
    private final StringBuilder str;
    public StringSendObject(String data) {
        str = new StringBuilder(data);
    }
    public StringSendObject() {
        str = new StringBuilder();
    }
    
    public void write(String data) {
        str.append(data);
    }
    
    @Override protected byte[] data() {
        try {
            return str.toString().getBytes("UTF8");
        } catch (UnsupportedEncodingException ex) {
            return new byte[0];
        }
    }

}
