/*
 * Java Simple network library
 * Copyright (C) December 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import threadManagement.Killable;

/**
 * This class holds a ServerSocket, and automates the process
 * of listening for new clients, and assigning their connections
 * a NetworkInterface.
 * @author wilson
 */
public abstract class SimpleNetworkServer implements Killable {
    private final Object sync = new Object(); // The sync object for status
    // A list of all the currently connected clients
    private final LinkedList<NetworkInterface> clients = new LinkedList<NetworkInterface>();
    // Whether this server is listening for connections
    private volatile boolean listening = false;
    private ServerSocket serverSocket; // The ServerSocket for connections
    private IOException recentException; // The most recent exception
    
    
    /**
     * This is the current id used for assigning an id to connecting clients.
     */
    private long currentClientId = 0;
    
    private final LinkedList<NetworkInterfaceChangeHandler> newClientHandlers = new LinkedList<NetworkInterfaceChangeHandler>();
    private final LinkedList<NetworkInterfaceChangeHandler> clientLeaveHandlers = new LinkedList<NetworkInterfaceChangeHandler>();
    
    
    /**
     * Starts the server listening for incoming connections.
     * @param port The port number to listen on.
     * @return Returns whether successful.
     */
    public boolean listen(int port) {
        synchronized(sync) {
            if (listening) return false;
            try {
                serverSocket = new ServerSocket(port);
                listening = true;
                connectionsThread.start();
                return true;
            } catch(IOException io) {
                recentException = io;
            }
        }
        kill(); return false;
    }
    
    
    public boolean isListening() {
        synchronized(sync) {
            return listening;
        }
    }
    
    /**
     * This method kills the connection to a client, in the neatest possible way,
     * this can be used to kick a client, or to end the connection with a client
     * that is not responding/is responding incorrectly,
     * this method uses synchronisation, so can be called by any thread at any time.
     * @param client The NetworkInterface client to kill - it must be actively
               stored by this server instance, or the method will do nothing.
     */
    public void killClient(NetworkInterface client) {
        synchronized(sync) {
            if (clients.contains(client)) {
                client.kill();
                clients.remove(client);
            }
        }
    }
    
    /**
     * Removes a client from the clients list, this should only be run by NetworkInterface,
     * when it's kill method is run, as such it is final,
     * if you simply want to kick/disconnect a client, use killClient instead.
     * @param client 
     */
    void removeClientFromClientsList(NetworkInterface client) {
        synchronized(sync) {
            if (clients.contains(client)) clients.remove(client);
        }
    }
    
    /**
     * Kills the ServerSocket associated with this server object, the thread for reading it,
     * and the connections of any clients currently connected,
     * this can also be called at any time, as it uses synchronisation.
     */
    @Override public void kill() {
        while(!clients.isEmpty()) { // Do this instead of for loop, to avoid ConcurrentModificationExceptions,
            killClient(clients.getFirst()); // and so all clients are actually killed.
        }
        synchronized(sync) {
            listening = false;
            connectionsThread.interrupt();
            if (serverSocket != null)
                try {
                    serverSocket.close();
                } catch(IOException io) {
                } finally {
                    serverSocket = null;
                }
        }
        synchronized(killLock) { killLock.notify(); }
    }
    
    private final Object killLock = new Object();
    @Override public void waitForEnd() {
        while(listening)
            try { synchronized(killLock) { killLock.wait(); } } catch(InterruptedException ie) {}
    }
    
    @Override public boolean isRunning() { return listening; }
    
    
    /**
     * Gets the most recent error.
     * @return The most recent IOException thrown and caught within this object's internal methods - e.g: connection error.
     */
    public IOException getRecentException() {
        synchronized(sync) {
            return recentException;
        }
    }
    
    /**
     * Gets a duplicate list of all the currently connected clients, this can be iterated through
     * without generating ConcurrentModificationExceptions, as it is a duplicate of the list,
     * 
     * Likewise, any modifications to the list returned will not affect the clients connected to the server.
     * @return 
     */
    public LinkedList<NetworkInterface> getClients() {
        synchronized(sync) {
            return new LinkedList<NetworkInterface>(clients);
        }
    }
    
    
    /**
     * Implementing classes must provide this method - it must create
     * an abstract NetworkInterface, for use by this server.
     * @return 
     */
    public abstract NetworkInterface createInterface();
    
    
    
    
    /**
     * Adds a new new client handler.
     * A new client handler will be run BEFORE THE READ THREAD STARTS.
     * This means it can add dataHandlers, and it is ensured that they will run.
     * @param l 
     */
    public void addNewClientHandler(NetworkInterfaceChangeHandler l) {
        synchronized(sync) {
            newClientHandlers.add(l);
        }
    }
    
    /**
     * Adds a new client leave handler.
     * @param l 
     */
    public void addClientLeaveHandler(NetworkInterfaceChangeHandler l) {
        synchronized(sync) {
            clientLeaveHandlers.add(l);
        }
    }
    
    
    
    
    // ================================== THE "WAIT FOR CONNECTIONS" THREAD ================
    
    
    private final ConnectionsThread connectionsThread = new ConnectionsThread();
    
    private class ConnectionsThread extends Thread {
        private ConnectionsThread() {
            super("SimpleNetworkServer connections thread");
        }
        @Override public void run() {
            while(listening) { // Loop while supposed to be listening
                try {
                    Socket c = serverSocket.accept(); // Accept any incoming connections
                    NetworkInterface n = createInterface(); // Create an interface
                    synchronized(sync) { // Synchronise, as changing status of clients list
                        n.id = currentClientId++; // Set the id
                        
                        n.addConnectHandlers(newClientHandlers); // Add connect and leave handlers
                        n.addLeaveHandlers(clientLeaveHandlers);
                        
                        if (n.connectToSocket(c,SimpleNetworkServer.this)) { // Get the client to connect to the socket
                            clients.add(n); // Add it to the list, if successful
                        } else {
                            if (n.getRecentException() != null)
                                n.getRecentException().printStackTrace();
                            n.kill(); // Kill the client's connection - to make sure it is tidied up
                            c.close(); // Close the socket, just to make sure - this should have already been done by the client's killConnection()
                        }
                    }
                } catch(IOException io) {
                    recentException = io;
                }
            }
            System.out.println("Server connections thread ended.");
        }
    }
    
}
