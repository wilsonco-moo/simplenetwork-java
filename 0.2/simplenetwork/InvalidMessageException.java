/*
 * Java Simple network library
 * Copyright (C) October 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork;

/**
 * This exception can be thrown from within an ErrorDetectingNetworkInterface's data handler
 * object. If this is thrown from within an acknowledged message's data handler body, this
 * will stop the message being confirmed as acknowledged. Thus, the message will be sent again.
 * This must only be thrown in the event of a suspected network error, as it will cause the
 * same message to be sent again.
 * @author wilson
 */
public class InvalidMessageException extends UnsupportedOperationException {
    public InvalidMessageException() {
        super("Exception: Invalid message.");
    }
    public InvalidMessageException(String msg) {
        super(msg);
    }
}
