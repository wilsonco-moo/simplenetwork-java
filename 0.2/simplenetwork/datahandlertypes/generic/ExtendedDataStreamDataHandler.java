/*
 * Java Simple network library
 * Copyright (C) October 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package simplenetwork.datahandlertypes.generic;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import simplenetwork.DataHandler;
import simplenetwork.extendedStream.ExtendedDataInputStream;

/**
 * This data handler allows for an extended data handler to be constructed from the recieved data.
 * @author wilson
 */
public abstract class ExtendedDataStreamDataHandler extends DataHandler {
    @Override public void run(byte[] data) {
        if (data == null) data = new byte[0];
        ExtendedDataInputStream in = new ExtendedDataInputStream(new ByteArrayInputStream(data));
        process(in);
        try {
            in.close();
        } catch(IOException io) { io.printStackTrace(); }
    }
    public abstract void process(ExtendedDataInputStream data);
}




/**
 * NOTE: ALL FOLLOWING METHODS ARE REMOVED DEPRECATED METHODS,
 * THEY CAN NOW BE FOUND IN ExtendedDataInputStream.
 */
    
//                /**
//                 * This method reads a String encoded as an int length, then a set of UTF8 bytes, stored
//                 * within the data input stream, such as by DataStreamSendObject.writeLengthString()
//                 * this only does something when run from within the abstract process method.
//                 * @return 
//                 */
//                public String readLengthString() {
//                    try {
//                        return new String(readLengthBytes(),"UTF8");
//                    } catch(UnsupportedEncodingException ie) { ie.printStackTrace(); return ""; }
//                }
//                /**
//                 * This method reads a byte array encoded as an int length, then a series of bytes, stored
//                 * within the data input stream, such as by DataStreamSendObject.writeLengthBytes()
//                 * this only does something when run from within the abstract process method.
//                 * @return 
//                 */
//                public byte[] readLengthBytes() {
//                    if (in == null) return  new byte[0];
//                    try {
//                        int len = in.readInt();
//                        byte[] bytes = new byte[len];
//                        in.read(bytes,0,len);
//                        return bytes;
//                    } catch(IOException io) { io.printStackTrace(); return new byte[0]; }
//                }
//                /**
//                 * This method reads a int array encoded as an int length, then a series of integers, stored
//                 * within the data input stream, such as by DataStreamSendObject.writeLengthIntArray()
//                 * this only does something when run from within the abstract process method.
//                 * @return 
//                 */
//                public int[] readLengthIntArray() {
//                    if (in == null) return  new int[0];
//                    try {
//                        int len = in.readInt();
//                        int[] arr = new int[len];
//                        int i = 0; while(i < len) {
//                            arr[i] = in.readInt(); i++;
//                        }
//                        return arr;
//                    } catch(IOException io) { io.printStackTrace(); return new int[0]; }
//                }


/**
 * This is the code for the now deprecated and remove ChunkDataStreamDataHandler.
 */

//            /*
//             * To change this license header, choose License Headers in Project Properties.
//             * To change this template file, choose Tools | Templates
//             * and open the template in the editor.
//             */
//            package chunksystem.common.misc.data;
//
//            import simplenetwork.datamanagement.extendedStreams.ExtendedDataInputStream;
//            import java.io.ByteArrayInputStream;
//            import java.io.DataInputStream;
//            import java.io.IOException;
//            import simplenetwork.datamanagement.DataHandler;
//
//            /**
//             * This is an extension to the standard DataStreamDataHandler 
//             * @author wilson
//             */
//            public abstract class ChunkDataStreamDataHandler extends DataHandler {
//                @Override public void run(byte[] data) {
//                    try {
//                        ExtendedDataInputStream in = new ExtendedDataInputStream(new ByteArrayInputStream(data));
//                        process(in);
//                        in.close();
//                    } catch(IOException io) { io.printStackTrace(); }
//                }
//                public abstract void process(ExtendedDataInputStream data);
//            }


/**
 * This is the old deprecated code from DataStreamSendObject.
 */

//
//            public void writeDepthLong(long num, int len) {
//                    try {
//                        int i = 0, e = len*8;
//                        while(i < len) {
//                            data.writeByte((int)(num >> (e-=8) ));
//                            i++;
//                        }
//                    } catch(IOException io) { io.printStackTrace(); }
//                }
//                public void writeLengthString(String s) {
//                    try {
//                        writeLengthBytes(s.getBytes("UTF8"));
//                    } catch(UnsupportedEncodingException ie) { ie.printStackTrace(); }
//                }
//                public void writeLengthBytes(byte[] bytes) {
//                    if (sent) return;
//                    try {
//                        data.writeInt(bytes.length);
//                        data.write(bytes);
//                    } catch(IOException io) { io.printStackTrace(); }
//                }
//                public void writeLengthIntArray(int[] arr) {
//                    if (sent) return;
//                    try {
//                        data.writeInt(arr.length);
//                        for (int i : arr) {
//                            data.writeInt(i);
//                        }
//                    } catch(IOException io) {io.printStackTrace(); }
//                }
